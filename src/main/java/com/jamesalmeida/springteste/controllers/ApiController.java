package com.jamesalmeida.springteste.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jamesalmeida.springteste.models.Dados;

@RestController
public class ApiController {
	
	@RequestMapping("personagem")
	public Dados getDados() {
		Dados p = new Dados();
		
		p.personagem = "Batman";
		p.idade = 34;
		p.peso = 95.5;
		
		return p;
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/personagem")
	public ResponseEntity<Dados> setDados(@RequestBody Dados dados){
		
		return ResponseEntity.status(201).body(dados);
	}
}
